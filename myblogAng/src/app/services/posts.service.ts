import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PostsService {
  
  Url = 'https://api.gamebuz.co/posts';

  constructor(private http: HttpClient) { }

  getPosts(): Observable<any> {
      return this.http.get(this.Url);
  }

}