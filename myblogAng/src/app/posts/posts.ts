export class Posts {
    _featuredImage: string;
    title: string;
    shortDescription: string;
    createdAt: Date;
    _author: string;
}