import { Component, OnInit } from '@angular/core';
import { PostsService } from '../services/posts.service';
import { Posts } from './posts';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent {

  listPosts: Posts[];

  constructor(private _postsService: PostsService) { }

  ngOnInit() {
    this._postsService.getPosts()
        .subscribe(
          data =>
          {
            this.listPosts = data['data'];
          }
        );
  }

}
